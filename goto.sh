#!/usr/bin/env bash

gt() {

  [[ $@ == "" ]] && { goto_help; return $?; }
  [[ $1 == "add" ]] && { shift; goto_add $@; return $?; }
  [[ $1 == "edit" ]] && { $EDITOR $HOME/.config/goto; return $?; }

  PATTERN=$(cut -d / -f 1 <(echo $1))
  SUBDIR=$(cut -s -d / -f 2- <(echo $1))

  DEST=$(cat $HOME/.config/goto | goto.awk -v pattern=$PATTERN)
  DEST=$(sed "s+^\~+$HOME+" <(echo $DEST))

  if [[ $DEST == "" ]]; then 
    echo "pattern \"$1\" not found"
    return 1
  else
    DEST=$DEST/$SUBDIR
    cd $(realpath $DEST)
    return $?
  fi

}

goto_add() {

  EXISTS=$(cat $HOME/.config/goto | goto.add.check.awk -v pattern=$1)
  [[ $EXISTS == "true" ]] && { echo "$1 is already a shortcut."; return 1; }

  DEST=$(sed "s+^\~+$HOME+" <(echo $2))

  [ -d $DEST ] || { echo "$DEST: no such directory"; return 1; }
  echo "$1=$DEST" >> $HOME/.config/goto
  return $?

}

goto_help() {

  cat << EOF
usage:  gt <shortcut> ... to jump to a shortcut
        gt add <shortcut> <directory> ... to add a new shortcut
        gt edit ... to edit the shortcuts file with $EDITOR
EOF

  return 1

}
