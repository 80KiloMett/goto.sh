#!/usr/bin/awk -f

BEGIN { 
  FS="=" 
  if (pattern=="") {
    print "pattern not set";
    exit
    }
}
/^#/          { next }
$1==pattern   { print $2 }
