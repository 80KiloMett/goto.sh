goto.sh is a simple directory shortcut manager written in bash and awk

# Installation
Clone the repository and add the following lines to your shell's rc file (Change the file paths to point to your goto.sh installation). Then restart your shell.
```
alias goto.awk="/path/to/goto.awk" # this is used to parse the shortcut file ~/.config/goto
alias goto.add.check.awk="/path/to/goto.add.check.awk" # this is used to check if a shortcut already exists
source /path/to/goto.sh
```
Instead of aliasing the two awk-scripts you might also add them to your $PATH. Sourcing the goto.sh script is not optional, since it uses cd to change directories.

# Usage
Imagine this is your directory structure
```
/home/yourname
|
|-- Documents
|   |
|   |-- taxes
|       |-- 2019
|       |-- 2020
|       |-- 2021
|
|-- Code
    |-- project1
    |-- project2
```
You can use goto.sh via the command `gt`. 

To add new shortcuts just run `gt add <shortcut> <directory>`
```
$ gt add taxes ~/Documents/taxes
$ gt add project1 ~/Code/project1
$ gt add docs ~/Documents
```
To go to one of your shortcut directories just use `gt <shortcut>`
```
$ gt taxes
```
You can also append sub-directories:
```
$ gt taxes/2020
$ gt code/project2
```
All your shortcuts are saved in a text file `~/.config/goto` in a simple format:
```
taxes=/home/yourname/Documents/taxes
project1=/home/yourname/Code/project1
docs=/home/yourname/Documents
```
Instead of using `gt add` you can also edit this file by hand. The command `gt edit` opens the file in your $EDITOR. When editing by hand you do not need to write out the full path of your home-directory, just use `~`.
